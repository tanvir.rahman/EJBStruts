package com.revature.struts2;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class MyInterceptor extends AbstractInterceptor {

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		/* pre-processing */
		System.out.println("Pre-Processing");

		/* call action or next interceptor */
		String result = invocation.invoke();

		/* post-processing */
		System.out.println("Post-Processing");

		return result;
	}

}
