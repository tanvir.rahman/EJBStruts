<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!-- 
	The taglib directive tells the Servlet container that this page will be
	using the Struts 2 tags and that these tags will be preceded by s. The
	s:property tag displays the value of action class property "name> which
	is returned by the method getName() of the HelloWorldAction class. 
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Struts 2 Hello World</title>
</head>
<body>
	Hello World, this is <s:property value="name" /></br>
	Value of key 1 : <s:property value="key1" /></br>
	Value of key 2 : <s:property value="key2" /></br>

</body>
</html>