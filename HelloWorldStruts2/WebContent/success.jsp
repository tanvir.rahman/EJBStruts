<%@ page contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>File Upload Success</title>
</head>
<body>
	You have successfully uploaded: <s:property value="myFileFileName" /></br>
	User File upload: <s:property value="myFile" /></br>
	Content Type: <s:property value="myFileContentType" /></br>
	
	
</body>
</html>