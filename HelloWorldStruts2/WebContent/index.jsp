<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Hello World</title>
</head>
<body>
	<div class="col-sm-6 col-md-6">
		<h1>Hello World from Struts2</h1>
		<form action="hello">
			<label for="name">Please enter your name</label><br> 
			<input type="text" name="name" placeholder="Please enter your name" /> 
			<input type="submit" value="Say Hello" />
		</form>
		</br> The <b>hello</b> action defined in the above view file will be mapped
		to the HelloWorldAction class and its execute method using <b>struts.xml</b>
		file. When a user clicks on the Submit button it will cause the Struts
		2 framework to run the execute method defined in the HelloWorldAction
		class and based on the returned value of the method, an appropriate
		view will be selected and rendered as a response.
	</div>
	
	<div class="col-sm-6 col-md-6">
		<form action="upload" method="post" enctype="multipart/form-data">
			<label for="myFile">Upload your file</label>
			<input type="file" name="myFile" />
			<input type="submit" value="Upload"/>
		</form>
	</div>
</body>
</html>