<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Hello World</title>
</head>
<body>
	<h1>Hello World from Struts</h1>
	<!-- action is what struts.xml looks at to see which action class to go to -->
	<!-- <form action="hello">  -->
	<s:form action="hello">
		<label for="name">Please enter your name</label><br>
		<!-- Name of input corresponds to variable name in action class it will be sent to -->
		<input type="text" name="doot"/><br>
		
		<label>What do you want to say?</label><br>
		<input type="text" name="msg"/><br>
		<input type="submit" value="Talk"/>
	</s:form>
	<!-- </form> -->
</body>
</html>