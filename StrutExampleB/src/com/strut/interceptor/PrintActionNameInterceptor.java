package com.strut.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class PrintActionNameInterceptor implements Interceptor{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String intercept(ActionInvocation arg0) throws Exception {

		System.out.println(arg0.getAction().getClass() + " Starting");
		
		String result = arg0.invoke();
		System.out.println(arg0.getAction().getClass() + " ending");
		return result;
	}

}
