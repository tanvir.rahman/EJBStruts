package com.strut.action;

import com.opensymphony.xwork2.ActionSupport;

public class HelloWorldAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8372952383876696843L;
	private String doot;
	private String msg;
	public String execute(){
		System.out.println("no arg");
		if(doot.isEmpty() || msg.isEmpty()) return ERROR;
		return SUCCESS;
	}
	
	public String getDoot() {
		return doot;
	}
	public void setDoot(String doot) {
		this.doot = doot;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	

}
