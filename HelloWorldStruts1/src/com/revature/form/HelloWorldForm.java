package com.revature.form;

import org.apache.struts.action.ActionForm;

public class HelloWorldForm extends ActionForm {

	private static final long serialVersionUID = 1L;

	private String message;
	private int num = 0;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

}
