package com.revature.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.revature.form.HelloWorldForm;

public class HelloWorldAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HelloWorldForm hwform = (HelloWorldForm) form;

		// populate the pojo/form object in here
		hwform.setMessage("Hello World, this is coming from HelloWorldAction");
		if (Math.random() > 0.5) {
			return mapping.findForward("failure");
		}
		hwform.setNum(hwform.getNum()+1);
		return mapping.findForward("success");
	}

}
