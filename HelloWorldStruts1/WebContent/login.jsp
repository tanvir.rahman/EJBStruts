<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html>
<head>
<title>Login page</title>
</head>
<body>
	<div style="color: red">
		<html:errors />
	</div>

	<html:form action="/Login">
		User name : <html:text name="LoginForm" property="userName" />
		Password : <html:password name="LoginForm" property="password" />
		<html:submit value="Login" />
	</html:form>

</body>
</html>